
provider "kubernetes" {
  config_path = "kubeconfig"
}

provider "helm" {
  kubernetes {
    config_path = "kubeconfig"
  }
}

resource "kubernetes_namespace" "ns" {
  depends_on = [module.k3s, null_resource.fetch_kubeconfig]
  metadata {
    name = "cake"
  }
}

resource "helm_release" "chart" {
  depends_on = [kubernetes_namespace.ns, null_resource.fetch_kubeconfig]
  repository = "https://gitlab.com/api/v4/projects/38759792/packages/helm/stable"
  chart = "cake-manager"
  name  = "cake-manager"
  namespace = "cake"
  version = "0.3.0"
  atomic = true
  
  set {
    name  = "ingress.enabled"
    value = "true"
  }
  
  set {
    name = "global.baseDomain"
    value = var.domain
  }
}