
variable "justme_sg_id" {
  type = string
}

variable "default_sg_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "extra_subnet_id" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "domain" {
  type = string
}

variable "domain_zone_id" {
  type = string
}

variable "waf_rule_arn" {
  type = string
}
