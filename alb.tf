

module "allow_all_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"

  name        = "public"
  vpc_id      = var.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules = ["https-443-tcp"]
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 6.0"

  name = "cake-alb"

  load_balancer_type = "application"

  vpc_id             = var.vpc_id
  subnets            = [var.subnet_id, var.extra_subnet_id]
  security_groups    = [var.default_sg_id, var.justme_sg_id, module.allow_all_sg.security_group_id]

  target_groups = [
    {
      name_prefix      = "pref-"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
      targets = {
        my_target = {
          target_id = module.ec2_instance.id
          port = 80
        }
      }
      health_check = {
        matcher = "200,404"
      }
    }
  ]

  https_listeners = [
    {
      port               = 443
      protocol           = "HTTPS"
      certificate_arn    = module.cert.acm_certificate_arn
      target_group_index = 0
    }
  ]

  http_tcp_listeners = [
    {
      port        = 80
      protocol    = "HTTP"
      action_type = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  ]

}

resource "aws_wafv2_web_acl_association" "waf_assoc" {
  resource_arn = module.alb.lb_arn
  web_acl_arn  = var.waf_rule_arn
}