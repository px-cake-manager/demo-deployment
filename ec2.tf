
module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "4.0.0"

  name = "cake-manager-demo"

  ami                    = "ami-0510e632102e49c91"
  instance_type          = "t3a.large"
  monitoring             = false
  vpc_security_group_ids = [var.justme_sg_id, var.default_sg_id]
  subnet_id              = var.subnet_id
  cpu_credits = "standard"
  key_name               = aws_key_pair.awskeypair.key_name

  create_spot_instance = false

  tags = {
    Terraform   = "true"
  }
}
