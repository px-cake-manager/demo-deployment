
resource "aws_route53_record" "dns" {
  name    = var.domain
  type    = "A"
  zone_id = var.domain_zone_id
  alias {
    evaluate_target_health = false
    name                   = module.alb.lb_dns_name
    zone_id                = module.alb.lb_zone_id
  }
}

resource "aws_route53_record" "dns2" {
  name    = "*.${var.domain}"
  type    = "A"
  zone_id = var.domain_zone_id
  alias {
    evaluate_target_health = false
    name                   = module.alb.lb_dns_name
    zone_id                = module.alb.lb_zone_id
  }
}