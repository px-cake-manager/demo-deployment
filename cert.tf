
module "cert" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 3.0"

  domain_name  = var.domain
  zone_id      = var.domain_zone_id
  subject_alternative_names = ["*.${var.domain}"]
  
  wait_for_validation = true
}